import numpy as np
from binance_bot import BinanceBot
from overrides import overrides

class RandomBot(BinanceBot):
    def __init__(self, apiFilePath:str):
        super().__init__(apiFilePath)
        self.t = 0

    @overrides
    def makeNewActions(self, balance, prices):
        self.t += 1
        if self.t % 10 == 0:
            return self.sellAll(balance)

        # 60% chance of buying if we have some spare BUSD
        actionType = ["BUY", "SELL"][np.random.rand() <= 0.6] if "BUSD" in balance else "SELL"
        if actionType == "SELL":
            validCoins = []
            for k, v in balance.items():
                if prices["%sBUSD" % k] * v[0] > 5:
                    validCoins.append(k)
            pickedCoin = np.random.choice(validCoins)
            maxCount = balance[pickedCoin][0]
        else:
            # Just xxxBUSD
            busdCoins = list(filter(lambda x : x.endswith("BUSD"), prices.keys()))
            pickedCoin = np.random.choice(busdCoins)[0 : -4]
            maxCount = balance["BUSD"][0] * 1 / prices["%sBUSD" % pickedCoin]

        if pickedCoin == "BUSD":
            return [("PASS", None, None, None, None)]
        pickedValue = (np.random.rand() * 0.9 + 0.1) * maxCount
        return [(actionType, pickedCoin, "BUSD", pickedValue, None)]

    def __str__(self):
        Str = "[Random Bot]"
        Str += "\n - Api file path: %s" % self.apiFilePath
        return Str